// script.js
document.addEventListener("DOMContentLoaded", function () {
    const messageArea = document.getElementById("message-container");
    const messageInput = document.getElementById("message-input");
    const sendButton = document.getElementById("send-button");
    const editButton = document.getElementById("btn_edit");
    const deleteButton = document.querySelector(".btn_delete");

    //Adding event to buttons
    sendButton.addEventListener("click", sendMessage);
    editButton.addEventListener("click", editMessage);
    deleteButton.addEventListener("click", deleteMessage);

    let editedMessageElement = null;
    // Function to send a message
    function sendMessage() {
        // Get the message content typed by the user
        const message = messageInput.value.trim();
        if (message !== "") {
            if (editedMessageElement) {
               // Update edited message
                editedMessageElement.textContent = "Você: " + message;
                editedMessageElement = null;
                replyToMessage("Você editou a mensagem para: \"" + message + "\"");
            } else {
               // Create a new message
                const messageElement = document.createElement("div");
                messageElement.className = "message user-message";

                const messageContent = document.createElement("span");
                messageContent.textContent = "Você: " + message;
                messageElement.appendChild(messageContent);
                // Add the new message to the message area
                messageArea.appendChild(messageElement);
                // Simulate ChatBot response to new message
                replyToMessage("Você enviou a mensagem: \"" + message + "\"");
            }
            // Clear the input box and enable it to type a new message
            messageInput.value = "";
            messageInput.disabled = false;
            messageInput.focus();
        }
    }
    // Function to edit the last message sent by the user
    function editMessage() {
        const messages = messageArea.querySelectorAll(".user-message");
        if (messages.length > 0) {
            messageInput.disabled = false;
            messageInput.focus();

            editedMessageElement = messages[messages.length - 1];
            const messageContent = editedMessageElement.textContent.split(": ")[1];
            messageInput.value = messageContent;

            // Remover a mensagem original quando o usuário editar
            messageArea.removeChild(editedMessageElement);
        }
    }
    // Function to delete the last sent message or cancel editing
    function deleteMessage() {
        if (editedMessageElement) {
            editedMessageElement = null;
            messageInput.value = "";
            messageInput.disabled = false;
            return;
        }
        // Get all messages
        const messages = messageArea.querySelectorAll(".message");
        if (messages.length > 0) {
            // remove the last message
            const lastMessage = messages[messages.length - 1];
            messageArea.removeChild(lastMessage);
        }
    }
    // Function to simulate a ChatBot response
    function replyToMessage(message) {
        setTimeout(function () {
            const reply = "ChatBot: " + message;
            // Create a div for the ChatBot response
            const replyElement = document.createElement("div");
            replyElement.className = "message";
            replyElement.textContent = reply;
            // Add ChatBot's response to message area after 0.5s
            messageArea.appendChild(replyElement);
        }, 500);
    }
});
